package pl.oblivion.model.mesh;

public interface Mesh {

  float[] getVertices();

  int[] getIndices();

  int getIndexCount();
}
